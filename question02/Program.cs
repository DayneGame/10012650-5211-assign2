﻿using System;

namespace question02
{
    class Program
    {
        

        static void Main(string[] args)
        {
            string[] fruits = new string[4] {"Apple", "Banana", "Coconut", "Mandarin"};

            foreach(var fruit in fruits) {
                // Console.Write outputs text onto a single standard output stream (1 Line).
                // Console.WriteLine writes the specified data, followed by the current line terminator, to the standard output stream.

                Console.WriteLine(fruit); 
                // Apple
                // Banana
                // Coconut
                // Mandarin
            }
        }
    }
}
