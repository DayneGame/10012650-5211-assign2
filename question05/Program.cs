﻿using System;

namespace question05
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Print out the division table for:");

            var number = int.TryParse(Console.ReadLine(), out int inputNum);
            var doubleNum = Convert.ToDouble(inputNum);
            string formatNum = string.Format("{0:00}", doubleNum);

            for(var i = 0; i < 12; i++) {
                var a = i + 1;
                var calculation = a / doubleNum;
                var roundCal = Math.Round(calculation, 2);
                
                string formatA = string.Format("{0:00}", a);

                if (a <= 10 | calculation <= 10 | doubleNum <= 10){
                    Console.WriteLine($"{formatA} / {formatNum} = 0{roundCal}");
                } else {
                    Console.WriteLine($"{a} / {inputNum} = {roundCal}");
                }
            }
        }
    }
}
