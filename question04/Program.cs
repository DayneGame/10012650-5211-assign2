﻿using System;

namespace question04
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Print out the reverse timestables (highest number first) for:");

            var isnumber = int.TryParse(Console.ReadLine(), out int inputNum);

            for(var i = 11; i >= 0; i--) {
                var a = i + 1;
                var calculation = a * inputNum;
                Console.WriteLine($"{a} x {inputNum} = {calculation}");
            }
        }
    }
}
